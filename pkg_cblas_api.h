/// @file pkg_cblas_api.h
///
/// CBLAS package API
///

#pragma once

#ifdef __cplusplus
extern "C" {
#endif
/// Initialize blas library
#ifdef PKG_CBLAS_LIB_ARM_GEMM
extern void arm_cblas_init();
#define PkgCblasLibraryInit() arm_cblas_init()
#else
static inline void PkgCblasLibraryInit() {}
#endif


/// @return name of the blas library used
static inline const char *PkgCblasLibraryName() {
#ifdef PKG_CBLAS_LIB_NAME
  return PKG_CBLAS_LIB_NAME;
#else
#error 'Unknown CBLAS library'
#endif
   // Avoid warning
  return "none";
}

#ifdef __cplusplus
}
#endif
