/// @file pkg_cblas.h
///
/// CBLAS wrapper
///

#pragma once

// Make sure only one BLAS library is enabled.
#if defined(PKG_CBLAS_LIB_OPENBLAS) + \
  defined(PKG_CBLAS_LIB_ATLAS) + \
  defined(PKG_CBLAS_LIB_MKL) + \
  defined(PKG_CBLAS_LIB_BLIS) + \
  defined(PKG_CBLAS_LIB_ARM_GEMM) > 1
#error "Cannot use more than one BLAS library."
#endif

#if defined(PKG_CBLAS_LIB_OPENBLAS)
#include <openblas/cblas.h>
#endif

#if defined(PKG_CBLAS_LIB_ACCELERATE)
#include <cblas.h>
typedef enum CBLAS_ORDER CBLAS_ORDER;
typedef enum CBLAS_TRANSPOSE CBLAS_TRANSPOSE;
#endif


#if defined(PKG_CBLAS_LIB_ATLAS)
#include <atlas/cblas.h>
typedef enum CBLAS_ORDER CBLAS_ORDER;
typedef enum CBLAS_TRANSPOSE CBLAS_TRANSPOSE;
#endif

#if defined(PKG_CBLAS_LIB_MKL)
#include <mkl/mkl.h>
#endif

#if defined(PKG_CBLAS_LIB_ARM_GEMM)
#include "arm_blas.h"
#endif

#if defined(PKG_CBLAS_LIB_BLIS)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-function"
#include <blis/cblas.h>
#pragma GCC diagnostic pop
typedef enum CBLAS_ORDER CBLAS_ORDER;
typedef enum CBLAS_TRANSPOSE CBLAS_TRANSPOSE;
#endif
