# Generic cblas wrapper

Provides a CBLAS interface by using one of several supported BLAS libraries.
Selection of the actual CBLAS library to be used is done at build-time.


